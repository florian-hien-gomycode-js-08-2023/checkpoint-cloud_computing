import React from 'react'
import GoogleMapReact from 'google-map-react';


const AnyReactComponent = ({ text }) => <div>{text}</div>;
export default function Map() {
  const defaultProps = {
    center: {
      lat: 7.9897371,
      lng: 	-5.5679458,
    },
    zoom: 7
  };

  return (
    <div className='container my-5'>
      <div style={{ height: '50vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyCBSHtVRbvcM7DuXwelqdFHSkGH4LFtoGI" }}
          defaultCenter={defaultProps.center}
          defaultZoom={defaultProps.zoom}
        >
          <AnyReactComponent
            lat={defaultProps.center.lat}
            lng={defaultProps.center.lng}
            // text="My Marker"
          />
        </GoogleMapReact>
      </div>
    </div>
  )
}
