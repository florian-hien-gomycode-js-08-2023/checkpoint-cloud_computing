import React from 'react';

export default function Header() {
  return (
    <div className="container mt-md-4 mt-2">
      <div className='text-center mb-md-5 mb-2'><h1>WELCOME TO IVORY COAST</h1></div>
      <div className="row gap-2 align-items-center">
        <div className="col-md-4">
          <div>
            <img src="https://images.pexels.com/photos/5985284/pexels-photo-5985284.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" srcset="" style={{ width: '100%', minWidth:'200px' }} />
          </div>
        </div>
        <div className="col-md-7">
          <div>
            <p>Ivory Coast, a West African country and member of the African Union, covers an expansive 322,462 km2. It shares borders with Mali and Burkina Faso to the north, Liberia and Guinea to the west, Ghana to the east, and the Atlantic Ocean to the south. The population was estimated at 22,671,331 in 2014. The political capital is Yamoussoukro, while Abidjan serves as the economic capital. French is the official language, and the currency is the CFA franc. Ivory Coast is an active participant in ECOWAS, with French spoken by about 70% of its population.</p>
            <p>Boasting robust economic assets, Ivory Coast stands as a regional power. Inheriting infrastructure from the "Ivorian miracle" (1960-1980), the country hosts Sub-Saharan Africa's second-largest port, an extensive road network, and a recently expanded international airport. Within the agricultural sector, Ivory Coast leads the world in cocoa production, holding over 40% of the market. It also excels in other export-oriented agricultural productions, including rubber, cashews, cotton, coffee, oil palm, bananas, pineapples, and cola.</p>
            <p>The secondary sector is marked by dominance in crude oil refining, construction, engineering, and agro-food processing. The tertiary sector, comprising 47% of the GDP, is heavily influenced by banking, transportation, distribution, and information and communication technologies (ICT), notably mobile telephony with five operators. Ivory Coast achieves energy self-sufficiency through the exploitation of gas and oil deposits, enabling the export of electricity and petroleum products to the sub-region.</p>
            <p>In summary, Ivory Coast stands at the intersection of economic prowess and regional influence, supported by a diverse range of industries, strong infrastructure, and strategic participation in international organizations.</p>
          </div>
        </div>
      </div>
    </div>
  );
}
