import Map from './Components/Map';
import Header from './Components/Header';

function App() {

  return (
    <div className="App">
      <Header/>
      <Map/>
    </div>
  );
}

export default App;
